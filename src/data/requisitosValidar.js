export const requisitosValidar = [
  {
    idRequisitoCae: "a25c8589-e907-4af5",
    idRequisito: "a25c-lugares",
    idEvidence: "b400054e-e414-44a5",
    requisito: "Evaluacion lugares",
    origen: "Servicio",
    fechaSubida: "22-05-2022",
    nombre: "C.P. Winterfell",
  },
  {
    idRequisitoCae: "a25c8589-e907-4a6775",
    idRequisito: "a25c-ficha-cae123",
    idEvidence: "b400054e-e414-4461",
    requisito: "Ficha Cae",
    origen: "Servicio",
    fechaSubida: "22-06-2022",
    nombre: "C.P. Sion",
  },
  {
    idRequisitoCae: "a25c8589-e907-9812901",
    idRequisito: "a25c-recon-med",
    idEvidence: "b4012e-1234-4461",
    requisito: "Reconocimiento médico",
    origen: "Trabajador",
    fechaSubida: "21-04-2022",
    nombre: "Esteban Quito",
  },
];
